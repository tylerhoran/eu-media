EUROLAB 2012 INTERNET USE AND ECONOMIC VALUES STUDY
Tyler J Horan

Project utilizes TexShop (v3.11), R (v2.15.0)

To recreate research:

1. Register and Download the Eurobarometer 72.4 (ZA5449) dataset from:
http://zacat.gesis.org/webview/

2. Include the downloaded file in this directory.

3. Run load_and_clean_data.r, regression_tables.r, and graphs.r to create tables and graphs for the paper and presentation.

4. Copy and format output into the appropriate tex files. 