#EUROLAB 2012 INTERNET USE AND ECONOMIC VALUES STUDY
require(plyr)
require(Hmisc)
require(psych)
require(tikzDevice)

int.wrk.data = read.csv( file='int-wrk-data.csv', header=TRUE, sep=',')
int.wrk.data$gender = as.factor(int.wrk.data$gender)
liberal = subset(int.wrk.data, int.wrk.data$partisanship == 1)
conserv = subset(int.wrk.data, int.wrk.data$partisanship == -1)
centrist = subset(int.wrk.data, int.wrk.data$partisanship == 0)

#Dividing the Data By Region
eastern.europe = subset(int.wrk.data, int.wrk.data$country == 28 | int.wrk.data$country == 19 | int.wrk.data$country == 21 | int.wrk.data$country == 25 | int.wrk.data$country == 29 | int.wrk.data$country == 26)
western.europe = subset(int.wrk.data, int.wrk.data$country == 13 | int.wrk.data$country == 1 | int.wrk.data$country == 8 | int.wrk.data$country == 3 | int.wrk.data$country == 4 | int.wrk.data$country == 11 | int.wrk.data$country == 12 | int.wrk.data$country == 35)
northern.europe = subset(int.wrk.data, int.wrk.data$country == 2 | int.wrk.data$country == 20 | int.wrk.data$country == 7 | int.wrk.data$country == 9 | int.wrk.data$country == 34 | int.wrk.data$country == 22 | int.wrk.data$country == 23 | int.wrk.data$country == 36 | int.wrk.data$country == 15 | int.wrk.data$country == 17 | int.wrk.data$country == 16)
southern.europe = subset(int.wrk.data, int.wrk.data$country == 5 | int.wrk.data$country == 10 | int.wrk.data$country == 27 | int.wrk.data$country == 24 | int.wrk.data$country == 14 | int.wrk.data$country == 18 | int.wrk.data$country == 30 | int.wrk.data$country == 31 | int.wrk.data$country == 32 | int.wrk.data$country == 34 | int.wrk.data$country == 6 )

#Dividing the Data by Poltical Leaning
eastern.liberal = subset(eastern.europe, eastern.europe$partisanship == 1)
eastern.conserv = subset(eastern.europe, eastern.europe$partisanship == -1)
eastern.centrist = subset(eastern.europe, eastern.europe$partisanship == 0)
western.liberal = subset(western.europe, western.europe$partisanship == 1)
western.conserv = subset(western.europe, western.europe$partisanship == -1)
western.centrist = subset(western.europe, western.europe$partisanship == 0)
northern.liberal = subset(northern.europe, northern.europe$partisanship == 1)
northern.conserv = subset(northern.europe, northern.europe$partisanship == -1)
northern.centrist = subset(northern.europe, northern.europe$partisanship == 0)
southern.liberal = subset(southern.europe, southern.europe$partisanship == 1)
southern.conserv = subset(southern.europe, southern.europe$partisanship == -1)
southern.centrist = subset(southern.europe, southern.europe$partisanship == 0)

#Creating a graph of personal job expectations ~ Internet use by Politics
tikz('pers-int-politics.tex', width=3.5, height=4)
int.pers = describe.by(int.wrk.data$pers.job.sit, int.wrk.data$internet, mat=TRUE)
liberal.pers = describe.by(liberal$pers.job.sit, liberal$internet, mat=TRUE)
conserv.pers = describe.by(conserv$pers.job.sit, conserv$internet, mat=TRUE)
centrist.pers = describe.by(centrist$pers.job.sit, centrist$internet, mat=TRUE)
plot(liberal.pers$mean, type='b', ylim=c(-0.2,0.2), xlab='Internet Use', ylab='Evaluation', lty=2)
lines(centrist.pers$mean, type='b', lty=1)
lines(conserv.pers$mean, type='b', lty=3)
legend("bottomright",legend=c("Left", "Center", "Right"), lty=c(2,1,3), bty='n')
dev.off()

#Creating a graph of personal job expectations ~ Internet use by Politics for presentation
tikz('pers-int-politics-pres.tex', width=2.625, height=3.5)
int.pers = describe.by(int.wrk.data$pers.job.sit, int.wrk.data$internet, mat=TRUE)
liberal.pers = describe.by(liberal$pers.job.sit, liberal$internet, mat=TRUE)
conserv.pers = describe.by(conserv$pers.job.sit, conserv$internet, mat=TRUE)
centrist.pers = describe.by(centrist$pers.job.sit, centrist$internet, mat=TRUE)
plot(liberal.pers$mean, type='b', ylim=c(-0.2,0.2), xlab='Internet Use', ylab='Evaluation', lty=2)
lines(centrist.pers$mean, type='b', lty=1)
lines(conserv.pers$mean, type='b', lty=3)
legend("bottomright",legend=c("Left", "Center", "Right"), lty=c(2,1,3), bty='n')
dev.off()

#Creating a graph of national economy situation ~ Internet use by Politics
tikz('nat-econ-politics.tex', width=3.5, height=4)
int.pers = describe.by(int.wrk.data$nat.econ, int.wrk.data$internet, mat=TRUE)
liberal.pers = describe.by(liberal$nat.econ, liberal$internet, mat=TRUE)
conserv.pers = describe.by(conserv$nat.econ, conserv$internet, mat=TRUE)
centrist.pers = describe.by(centrist$nat.econ, centrist$internet, mat=TRUE)
plot(liberal.pers$mean, type='b', ylim=c(-0.5,0.1), xlab='Internet Use', ylab='Evaluation', lty=2)
lines(centrist.pers$mean, type='b', lty=1)
lines(conserv.pers$mean, type='b', lty=3)
legend("topright",legend=c("Left", "Center", "Right"), lty=c(2,1,3), bty='n')
dev.off()

#Creating a graph of national economy situation ~ Internet use by Politics for presentation
tikz('nat-econ-politics-pres.tex', width=2.625, height=3.5)
int.pers = describe.by(int.wrk.data$nat.econ, int.wrk.data$internet, mat=TRUE)
liberal.pers = describe.by(liberal$nat.econ, liberal$internet, mat=TRUE)
conserv.pers = describe.by(conserv$nat.econ, conserv$internet, mat=TRUE)
centrist.pers = describe.by(centrist$nat.econ, centrist$internet, mat=TRUE)
plot(liberal.pers$mean, type='b', ylim=c(-0.5,0.1), xlab='Internet Use', ylab='Evaluation', lty=2)
lines(centrist.pers$mean, type='b', lty=1)
lines(conserv.pers$mean, type='b', lty=3)
legend("topright",legend=c("Left", "Center", "Right"), lty=c(2,1,3), bty='n')
dev.off()



#Creating a graph of national economy situation ~ Internet use by Region 
tikz('nat-econ-region.tex', width=3.5, height=4)
northern.europe.econ = describe.by(northern.europe$nat.econ, northern.europe$internet, mat=TRUE)
southern.europe.econ = describe.by(southern.europe$nat.econ, southern.europe$internet, mat=TRUE)
eastern.europe.econ = describe.by(eastern.europe$nat.econ, eastern.europe$internet, mat=TRUE)
western.europe.econ = describe.by(western.europe$nat.econ, western.europe$internet, mat=TRUE)
plot(northern.europe.econ$mean, type='b', ylim=c(-0.8,0.4), xlab='Internet Use', ylab='Evaluation', lty=2)
lines(southern.europe.econ$mean, type='b', lty=1)
lines(eastern.europe.econ$mean, type='b', lty=3)
lines(western.europe.econ$mean, type='b', lty=4)
legend("topright",legend=c("Northern", "Southern", "Eastern", "Western"), lty=c(2,1,3,4), bty='n')
dev.off()

#Creating a graph of national economy situation ~ Internet use by Region for Presentation
tikz('nat-econ-region-pres.tex', width=2.625, height=3.5)
northern.europe.econ = describe.by(northern.europe$nat.econ, northern.europe$internet, mat=TRUE)
southern.europe.econ = describe.by(southern.europe$nat.econ, southern.europe$internet, mat=TRUE)
eastern.europe.econ = describe.by(eastern.europe$nat.econ, eastern.europe$internet, mat=TRUE)
western.europe.econ = describe.by(western.europe$nat.econ, western.europe$internet, mat=TRUE)
plot(northern.europe.econ$mean, type='b', ylim=c(-0.5,0.1), xlab='Internet Use', ylab='Evaluation', lty=2)
lines(southern.europe.econ$mean, type='b', lty=1)
lines(eastern.europe.econ$mean, type='b', lty=3)
lines(western.europe.econ$mean, type='b', lty=4)
legend("topright",legend=c("Northern", "Southern", "Eastern", "Western"), lty=c(2,1,3,4), bty='n')
dev.off()

