#EUROLAB 2012 INTERNET USE AND ECONOMIC VALUES STUDY
#Creating Summary Statistics

var.names.sum = c("Age", "Gender", "\\hspace{0.5cm}Male", "\\hspace{0.5cm}Female", "Education", "Partisanship", "\\hspace{0.5cm}Left", "\\hspace{0.5cm}Center", "\\hspace{0.5cm}Right", "Occupation Level", "\\hspace{0.5cm}Self-Employed", "\\hspace{0.5cm}Managers", "\\hspace{0.5cm}Other White Collar", "\\hspace{0.5cm}Manual", "\\hspace{0.5cm}House Person", "\\hspace{0.5cm}Unemployed", "\\hspace{0.5cm}Retired", "\\hspace{0.5cm}Student", "TV Use", "Radio Use", "Print Use", "Internet Use", "SM Use", "NE Eval")

description = describe(int.wrk.data)
gender.desc = table(int.wrk.data$gender)
partisanship.desc = table(int.wrk.data$partisanship)
occupation.desc = table(int.wrk.data$occupation)


n.summary = c(formatC(round(description$n[1], 0), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(description$n[2], 0), big.mark=",", drop0trailing=TRUE, format="f"), "", "", formatC(round(description$n[3], 0), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(description$n[5], 0), big.mark=",", drop0trailing=TRUE, format="f"), "", "", "", formatC(round(description$n[6], 0), big.mark=",", drop0trailing=TRUE, format="f"), "", "", "", "", "", "", "", "", formatC(round(description$n[8], 0), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(description$n[9], 0), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(description$n[10], 0), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(description$n[11], 0), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(description$n[12], 0), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(description$n[13], 0), big.mark=",", drop0trailing=TRUE, format="f"))

percent.summary = c("", "", formatC(round((gender.desc[[1]]/description$n[2])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round((gender.desc[[2]]/description$n[2])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), "", "", formatC(round((partisanship.desc[[3]]/description$n[5])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round((partisanship.desc[[2]]/description$n[5])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round((partisanship.desc[[1]]/description$n[5])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), "", formatC(round((occupation.desc[[1]]/description$n[6])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round((occupation.desc[[2]]/description$n[6])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round((occupation.desc[[3]]/description$n[6])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round((occupation.desc[[4]]/description$n[6])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round((occupation.desc[[5]]/description$n[6])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round((occupation.desc[[6]]/description$n[6])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round((occupation.desc[[7]]/description$n[6])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round((occupation.desc[[8]]/description$n[6])*100 , 1), big.mark=",", drop0trailing=TRUE, format="f"), "", "", "", "", "", "")

mean.summary = c(formatC(round(mean(int.wrk.data$age, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",  formatC(round(mean(int.wrk.data$tv, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(mean(int.wrk.data$radio, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(mean(int.wrk.data$written.press, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(mean(int.wrk.data$internet, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(mean(int.wrk.data$soc.net, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(mean(int.wrk.data$nat.econ, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"))

sd.summary = c(formatC(round(sd(int.wrk.data$age, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",  formatC(round(sd(int.wrk.data$tv, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(sd(int.wrk.data$radio, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(sd(int.wrk.data$written.press, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(sd(int.wrk.data$internet, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(sd(int.wrk.data$soc.net, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(sd(int.wrk.data$nat.econ, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"))

mdn.summary = c(formatC(round(median(int.wrk.data$age, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",  formatC(round(median(int.wrk.data$tv, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(median(int.wrk.data$radio, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(median(int.wrk.data$written.press, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(median(int.wrk.data$internet, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(median(int.wrk.data$soc.net, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(median(int.wrk.data$nat.econ, na.rm=TRUE), 1), big.mark=",", drop0trailing=TRUE, format="f"))

summary.table = data.frame(Variable=var.names.sum, N=n.summary, '$\\%$'=percent.summary, M=mean.summary, SD=sd.summary, Mdn=mdn.summary)




var.names.reg = c("{\\it Demographics}", "\\hspace{0.5cm}Age", "\\hspace{0.5cm}Female", "\\hspace{0.5cm}Education", "\\hspace{0.5cm}Managers", "\\hspace{0.5cm}White Collar", "\\hspace{0.5cm}Manual", "\\hspace{0.5cm}House Person", "\\hspace{0.5cm}Unemployed", "\\hspace{0.5cm}Retired", "\\hspace{0.5cm}$R^{2}$ (\\%)", "{\\it Media}", "\\hspace{0.5cm}Television", "\\hspace{0.5cm}Radio", "\\hspace{0.5cm}Print Media", "\\hspace{0.5cm}Internet", "\\hspace{0.5cm}Social Media", "\\hspace{0.5cm}$\\deltaR^{2}$ (\\%)", "Total $R^{2}$ (\\%)")


#Creating the regression model for View on the national Economy by liberal left

#Model 1 - Demographics Only
liberal.model.1 = lm(liberal$nat.econ~liberal$age+liberal$gender+liberal$education+liberal$occupation)

#Model 2 - Demographics Plus Partisanship Plus Media
liberal.model.2 = lm(liberal$nat.econ~liberal$age+liberal$gender+liberal$education+liberal$occupation+liberal$tv+liberal$radio+liberal$written.press+liberal$internet+liberal$soc.net)

sum.lib.1 = summary(liberal.model.1)
sum.lib.2 = summary(liberal.model.2)

liberal.model.1.raw = read.table(textConnection(capture.output(sum.lib.1)[12:21]), fill = TRUE)
liberal.model.2.raw = read.table(textConnection(capture.output(sum.lib.2)[13:27]), fill = TRUE)

left.coef = c("", formatC(round(liberal.model.1.raw$V2[2:10], 4), big.mark=",", format="f"), formatC(round(sum.lib.1[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), "", formatC(round(liberal.model.2.raw$V2[11:15], 4), big.mark=",", format="f"), formatC(round((sum.lib.2[[9]]-sum.lib.1[[9]])*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(sum.lib.2[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"))

left.stars = c("", as.vector(liberal.model.1.raw$V6[2:10]), "", "", as.vector(liberal.model.2.raw$V6[11:12]), as.vector(liberal.model.2.raw$V7[13:14]), as.vector(liberal.model.2.raw$V7[15]),  "", "")

left.reg = paste(left.coef, left.stars, sep="")

#Creating the regression model for View on the national Economy by centrist

#Model 1 - Demographics Only
centrist.model.1 = lm(centrist$nat.econ~centrist$age+centrist$gender+centrist$education+centrist$occupation)

#Model 2 - Demographics Plus Partisanship Plus Media
centrist.model.2 = lm(centrist$nat.econ~centrist$age+centrist$gender+centrist$education+centrist$occupation+centrist$tv+centrist$radio+centrist$written.press+centrist$internet+centrist$soc.net)

sum.cntr.1 = summary(centrist.model.1)
sum.cntr.2 = summary(centrist.model.2)


cntr.model.1.raw = read.table(textConnection(capture.output(sum.cntr.1)[12:21]), fill = TRUE)
cntr.model.2.raw = read.table(textConnection(capture.output(sum.cntr.2)[14:28]), fill = TRUE)

cntr.coef = c("", formatC(round(cntr.model.1.raw$V2[2:10], 4), big.mark=",", format="f"), formatC(round(sum.cntr.1[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), "", formatC(round(cntr.model.2.raw$V2[11:15], 4), big.mark=",", format="f"), formatC(round((sum.cntr.2[[9]]-sum.cntr.1[[9]])*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(sum.cntr.2[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"))

cntr.stars = c("", as.vector(cntr.model.1.raw$V6[2:3]), as.vector(cntr.model.1.raw$V7[4]), as.vector(cntr.model.1.raw$V6[5:10]), "", "",  as.vector(cntr.model.2.raw$V6[11:12]), as.vector(cntr.model.2.raw$V7[13]), as.vector(cntr.model.2.raw$V6[14:15]), "", "")

cntr.reg = paste(cntr.coef, cntr.stars, sep="")

#Creating the regression model for View on the national Economy

#Model 1 - Demographics Only
conserv.model.1 = lm(conserv$nat.econ~conserv$age+conserv$gender+conserv$education+conserv$occupation)

#Model 2 - Demographics Plus Traditional Media
conserv.model.2 = lm(conserv$nat.econ~conserv$age+conserv$gender+conserv$education+conserv$occupation+conserv$tv+conserv$radio+conserv$written.press+conserv$internet+conserv$soc.net)

sum.con.1 = summary(conserv.model.1)
sum.con.2 = summary(conserv.model.2)

con.model.1.raw = read.table(textConnection(capture.output(sum.con.1)[12:21]), fill = TRUE)
con.model.2.raw = read.table(textConnection(capture.output(sum.con.2)[13:27]), fill = TRUE)

con.coef = c("", formatC(round(con.model.1.raw$V2[2:10], 4), big.mark=",", format="f"), formatC(round(sum.lib.1[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), "", formatC(round(con.model.2.raw$V2[11:15], 4), big.mark=",", format="f"), formatC(round((sum.con.2[[9]]-sum.con.1[[9]])*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(sum.con.2[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"))

con.stars = c("", as.vector(con.model.1.raw$V6[2:3]), as.vector(con.model.1.raw$V7[4]), as.vector(con.model.1.raw$V6[5:10]), "", "",  as.vector(con.model.2.raw$V6[11:15]), "", "")

con.reg = paste(con.coef, con.stars, sep="")

#Creating the Regression Table

partisan.reg.table = data.frame(Predictors=var.names.reg, Left=left.reg, Center=cntr.reg, Right=con.reg)



#Creating the regression model for View on the national Economy by Region

#Northern Europe
#Model 1 - Demographics Only
northern.model.1 = lm(northern.europe$nat.econ~northern.europe$age+northern.europe$gender+northern.europe$education+northern.europe$occupation+northern.europe$partisanship)

#Model 2 - Demographics Plus Media
northern.model.2 = lm(northern.europe$nat.econ~northern.europe$age+northern.europe$gender+northern.europe$education+northern.europe$occupation+northern.europe$partisanship+northern.europe$tv+northern.europe$radio+northern.europe$written.press+northern.europe$internet+northern.europe$soc.net)

nor.sum.1 = summary(northern.model.1)
nor.sum.2 = summary(northern.model.2)

nor.model.1.raw = read.table(textConnection(capture.output(nor.sum.1)[13:23]), fill = TRUE)
nor.model.2.raw = read.table(textConnection(capture.output(nor.sum.2)[23:30]), fill = TRUE)

nor.coef = c("", formatC(round(nor.model.1.raw$V2[2:11], 4), big.mark=",", format="f"), formatC(round(nor.sum.1[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), "", formatC(round(nor.model.2.raw$V2[4:6], 4), big.mark=",", format="f"), formatC(round(nor.model.2.raw$V2[8], 4), big.mark=",", format="f"), formatC(round(nor.model.2.raw$V2[10], 4), big.mark=",", format="f"), formatC(round((nor.sum.2[[9]]-nor.sum.1[[9]])*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(nor.sum.2[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"))

nor.stars = c("", as.vector(nor.model.1.raw$V6[2:3]), as.vector(nor.model.1.raw$V7[4]), as.vector(nor.model.1.raw$V6[5:10]), as.vector(nor.model.1.raw$V7[11]), "", "",  as.vector(nor.model.2.raw$V6[4:5]), as.vector(nor.model.2.raw$V1[7]), as.vector(nor.model.2.raw$V1[9]), "", "", "")

nor.reg = paste(nor.coef, nor.stars, sep="")

#Southern Europe
#Model 1 - Demographics Only
southern.model.1 = lm(southern.europe$nat.econ~southern.europe$age+southern.europe$gender+southern.europe$education+southern.europe$occupation+southern.europe$partisanship)

#Model 2 - Demographics Plus Media
southern.model.2 = lm(southern.europe$nat.econ~southern.europe$age+southern.europe$gender+southern.europe$education+southern.europe$occupation+southern.europe$partisanship+southern.europe$tv+southern.europe$radio+southern.europe$written.press+southern.europe$internet+southern.europe$soc.net)

sou.sum.1 = summary(southern.model.1)
sou.sum.2 = summary(southern.model.2)

sou.model.1.raw = read.table(textConnection(capture.output(sou.sum.1)[12:23]), fill = TRUE)
sou.model.2.raw = read.table(textConnection(capture.output(sou.sum.2)[23:30]), fill = TRUE)
names(sou.model.1.raw) = c("V1", "V2", "V3", "V4", "V5", "V6")

sou.coef = c("", formatC(round(sou.model.1.raw$V1[2:11], 4), big.mark=",", format="f"), formatC(round(sou.sum.1[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), "", formatC(round(sou.model.2.raw$V2[4:8], 4), big.mark=",", format="f"), formatC(round((sou.sum.2[[9]]-sou.sum.1[[9]])*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(sou.sum.2[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"))

sou.stars = c("", as.vector(sou.model.1.raw$V5[2]), as.vector(sou.model.1.raw$V6[3]), as.vector(sou.model.1.raw$V5[4:11]), "", "",  as.vector(sou.model.2.raw$V6[4:8]), "", "")

sou.reg = paste(sou.coef, sou.stars, sep="")

#Eastern Europe
#Model 1 - Demographics Only
eastern.model.1 = lm(eastern.europe$nat.econ~eastern.europe$age+eastern.europe$gender+eastern.europe$education+eastern.europe$occupation+eastern.europe$partisanship)

#Model 2 - Demographics Plus Media
eastern.model.2 = lm(eastern.europe$nat.econ~eastern.europe$age+eastern.europe$gender+eastern.europe$education+eastern.europe$occupation+eastern.europe$partisanship+eastern.europe$tv+eastern.europe$radio+eastern.europe$written.press+eastern.europe$internet+eastern.europe$soc.net)

east.sum.1 = summary(eastern.model.1)
east.sum.2 = summary(eastern.model.2)

east.model.1.raw = read.table(textConnection(capture.output(east.sum.1)[12:23]), fill = TRUE)
east.model.2.raw = read.table(textConnection(capture.output(east.sum.2)[24:29]), fill = TRUE)

east.coef = c("", formatC(round(east.model.1.raw$V2[2:11], 4), big.mark=",", format="f"), formatC(round(east.sum.1[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), "", formatC(round(east.model.2.raw$V2[2:6], 4), big.mark=",", format="f"), formatC(round((east.sum.2[[9]]-east.sum.1[[9]])*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(east.sum.2[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"))

east.stars = c("", as.vector(east.model.1.raw$V6[2:11]), "", "",  as.vector(east.model.2.raw$V6[2:6]), "", "")

east.reg = paste(east.coef, east.stars, sep="")

#Western Europe
#Model 1 - Demographics Only
western.model.1 = lm(western.europe$nat.econ~western.europe$age+western.europe$gender+western.europe$education+western.europe$occupation+western.europe$partisanship)

#Model 2 - Demographics Plus Media
western.model.2 = lm(western.europe$nat.econ~western.europe$age+western.europe$gender+western.europe$education+western.europe$occupation+western.europe$partisanship+western.europe$tv+western.europe$radio+western.europe$written.press+western.europe$internet+western.europe$soc.net)

west.sum.1 = summary(western.model.1)
west.sum.2 = summary(western.model.2)

west.model.1.raw = read.table(textConnection(capture.output(west.sum.1)[13:23]), fill = TRUE)
west.model.2.raw = read.table(textConnection(capture.output(west.sum.2)[22:29]), fill = TRUE)

west.coef = c("", formatC(round(west.model.1.raw$V2[1:10], 4), big.mark=",", format="f"), formatC(round(west.sum.1[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), "", formatC(round(west.model.2.raw$V2[4:6], 4), big.mark=",", format="f"), formatC(round(west.model.2.raw$V2[8:9], 4), big.mark=",", format="f"), formatC(round((west.sum.2[[9]]-west.sum.1[[9]])*100, 1), big.mark=",", drop0trailing=TRUE, format="f"), formatC(round(west.sum.2[[9]]*100, 1), big.mark=",", drop0trailing=TRUE, format="f"))

west.stars = c("", as.vector(west.model.1.raw$V6[1:10]), "", "", as.vector(west.model.2.raw$V6[4:5]), as.vector(west.model.2.raw$V1[7]), "", "", "", "")

west.reg = paste(west.coef, west.stars, sep="")

#EU Regions Regression Table
eu.names.reg = c("{\\it Demographics}", "\\hspace{0.5cm}Age", "\\hspace{0.5cm}Female", "\\hspace{0.5cm}Education", "\\hspace{0.5cm}Managers", "\\hspace{0.5cm}White Collar", "\\hspace{0.5cm}Manual", "\\hspace{0.5cm}House Person", "\\hspace{0.5cm}Unemployed", "\\hspace{0.5cm}Retired", "\\hspace{0.5cm}Partisanship", "\\hspace{0.5cm}$R^{2}$ (\\%)", "{\\it Media}", "\\hspace{0.5cm}Television", "\\hspace{0.5cm}Radio", "\\hspace{0.5cm}Print Media", "\\hspace{0.5cm}Internet", "\\hspace{0.5cm}Social Media", "\\hspace{0.5cm}$\\Delta R^{2}$ (\\%)", "Total $R^{2}$ (\\%)")

eu.reg.table = data.frame(Predictors=eu.names.reg, Nothern=nor.reg, Southern=sou.reg, Eastern=east.reg, Western=west.reg)




