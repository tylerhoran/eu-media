#EUROLAB 2012 INTERNET USE AND ECONOMIC VALUES STUDY
#Loading the Required Libraries
library(foreign)

#Import Scripts for Eurobaromater Studies (ZA5449 has bad country labels)
print("Loading Data File")
ZA5449 = suppressWarnings(read.spss(file="ZA5449_v1-1-0.sav", use.value.labels=FALSE, to.data.frame=TRUE, max.value.labels=Inf, trim.factor.names=FALSE))

#Extracting the Appropriate Variables from each of the Eurobarometer Studies & Combining into one file
int.wrk.data = data.frame(age=ZA5449$vd11, gender=ZA5449$d10,  education=ZA5449$vd8, married=ZA5449$d7c, partisanship=ZA5449$d1r1, occupation=ZA5449$c14, country=ZA5449$country1, tv=ZA5449$qd3_1, radio=ZA5449$qd3_2, written.press=ZA5449$qd3_3, internet=ZA5449$qd3_4, soc.net=ZA5449$qd3_5, nat.econ=ZA5449$qa4a_1, econ.sit=ZA5449$qa5a_2, pers.job.sit=ZA5449$qa5a_5, eu.pol.perform=ZA5449$qc11_1, nat.pol.perform=ZA5449$qc11_2, eu.bank.trust=ZA5449$qa17_4)

#Creating value labels for the new data 
pers.job.expect.names = data.frame(id=c(-1,0,1),value=c("Worse","Same","Better"))
economy.expect.names = data.frame(id=c(-1,0,1),value=c("Worse","Same","Better"))
media.use.names = data.frame(id=c(0,1,2,3,4,5,6),value=c("No Internet Access","Never","Less Often","Two or Three Times a Month","About Once a Week", "Two or Three Times a Week","Everyday/Almost Everyday"))
countries = data.frame(attributes(ZA5449$country1))
country.names = data.frame(id=c(1:36), value=rev(row.names(countries)))

#Clearing the workpace except for the selected Eurobarometer combined data file
originals = c("ZA5449")
rm(list = originals)

#Cleaning the data. Adding NAs for 'no response' and 'Don't know'

#Trust in Central Bank 
int.wrk.data$eu.bank.trust [int.wrk.data$eu.bank.trust  == 3] = NA
int.wrk.data$eu.bank.trust [int.wrk.data$eu.bank.trust  == 2] = 0

#Shortening Some Country Names
country.names$value = as.character(country.names$value)
country.names$value = replace(country.names$value, country.names$value=="NORWAY (NOT INCLUDED)", "NORWAY")
country.names$value = replace(country.names$value, country.names$value=="SWITZERLAND (NOT INCLUDED)", "SWITZERLAND")
country.names$value = replace(country.names$value, country.names$value=="CYPRUS (REPUBLIC)", "CYPRUS")

#Setting political views to between -1 (right) and 1 (left)
int.wrk.data$partisanship[int.wrk.data$partisanship == 2 ] = 0
int.wrk.data$partisanship[int.wrk.data$partisanship == 3 ] = -1
int.wrk.data$partisanship[int.wrk.data$partisanship == 4 ] = NA
int.wrk.data$partisanship[int.wrk.data$partisanship == 5 ] = NA
int.wrk.data$partisanship[int.wrk.data$partisanship == 9 ] = NA

#Setting occupation to a class scale
#int.wrk.data$occupation[int.wrk.data$occupation == 1 ] = NA
#int.wrk.data$occupation[int.wrk.data$occupation > 4 ] = NA
#int.wrk.data$occupation[int.wrk.data$occupation == 4 ] = 1
#int.wrk.data$occupation[int.wrk.data$occupation == 3 ] = 2
#int.wrk.data$occupation[int.wrk.data$occupation == 2 ] = 3

#Setting views on national economy situation to between -1 and 1
int.wrk.data$nat.econ[int.wrk.data$nat.econ == 9 ] = NA
int.wrk.data$nat.econ[int.wrk.data$nat.econ == 5 ] = NA
int.wrk.data$nat.econ[int.wrk.data$nat.econ == 2 ] = 6
int.wrk.data$nat.econ[int.wrk.data$nat.econ == 3 ] = 7
int.wrk.data$nat.econ[int.wrk.data$nat.econ == 4 ] = 8

int.wrk.data$nat.econ[int.wrk.data$nat.econ == 6 ] = 0.33333
int.wrk.data$nat.econ[int.wrk.data$nat.econ == 7 ] = -0.33333
int.wrk.data$nat.econ[int.wrk.data$nat.econ == 8 ] = -1

#Setting views on the economy, Houhold Finance and job situation to between -1 and 1
int.wrk.data$econ.sit[int.wrk.data$econ.sit == 5 ] = NA
int.wrk.data$econ.sit[int.wrk.data$econ.sit == 4 ] = NA
int.wrk.data$econ.sit[int.wrk.data$econ.sit == 3 ] = 0
int.wrk.data$econ.sit[int.wrk.data$econ.sit == 2 ] = -1

int.wrk.data$pers.job.sit [int.wrk.data$pers.job.sit  == 5 ] = NA
int.wrk.data$pers.job.sit [int.wrk.data$pers.job.sit  == 4 ] = NA
int.wrk.data$pers.job.sit [int.wrk.data$pers.job.sit  == 3 ] = 0
int.wrk.data$pers.job.sit [int.wrk.data$pers.job.sit  == 2 ] = -1

int.wrk.data$education [int.wrk.data$education == 0 ] = NA
int.wrk.data$education [int.wrk.data$education == 98 ] = NA
int.wrk.data$education [int.wrk.data$education == 97 ] = NA
int.wrk.data$education [int.wrk.data$education == 99 ] = NA

#Setting EU and Nation Crisis Policy Performance to between -1 and 1
int.wrk.data$eu.pol.perform[int.wrk.data$eu.pol.perform == 5 ] = NA
int.wrk.data$eu.pol.perform[int.wrk.data$eu.pol.perform == 9 ] = NA
int.wrk.data$eu.pol.perform[int.wrk.data$eu.pol.perform == 2 ] = 6
int.wrk.data$eu.pol.perform[int.wrk.data$eu.pol.perform == 3 ] = 7
int.wrk.data$eu.pol.perform[int.wrk.data$eu.pol.perform == 4 ] = 8

int.wrk.data$eu.pol.perform[int.wrk.data$eu.pol.perform == 6 ] = 0.33333
int.wrk.data$eu.pol.perform[int.wrk.data$eu.pol.perform == 7 ] = -0.33333
int.wrk.data$eu.pol.perform[int.wrk.data$eu.pol.perform == 8 ] = -1

int.wrk.data$nat.pol.perform[int.wrk.data$nat.pol.perform == 5 ] = NA
int.wrk.data$nat.pol.perform[int.wrk.data$nat.pol.perform == 9 ] = NA
int.wrk.data$nat.pol.perform[int.wrk.data$nat.pol.perform == 2 ] = 6
int.wrk.data$nat.pol.perform[int.wrk.data$nat.pol.perform == 3 ] = 7
int.wrk.data$nat.pol.perform[int.wrk.data$nat.pol.perform == 4 ] = 8

int.wrk.data$nat.pol.perform[int.wrk.data$nat.pol.perform == 6 ] = 0.33333
int.wrk.data$nat.pol.perform[int.wrk.data$nat.pol.perform == 7 ] = -0.33333
int.wrk.data$nat.pol.perform[int.wrk.data$nat.pol.perform == 8 ] = -1

#Reversing the data for internet use where 6 is every day and 1 is Less Often (no internet access = NA)
int.wrk.data$internet[int.wrk.data$internet == 8 ] = NA
int.wrk.data$internet[int.wrk.data$internet == 7 ] = 17
int.wrk.data$internet[int.wrk.data$internet == 6 ] = 16
int.wrk.data$internet[int.wrk.data$internet == 5 ] = 15
int.wrk.data$internet[int.wrk.data$internet == 4 ] = 14
int.wrk.data$internet[int.wrk.data$internet == 3 ] = 13
int.wrk.data$internet[int.wrk.data$internet == 2 ] = 12
int.wrk.data$internet[int.wrk.data$internet == 1 ] = 11

int.wrk.data$internet[int.wrk.data$internet == 11 ] = 6
int.wrk.data$internet[int.wrk.data$internet == 12 ] = 5
int.wrk.data$internet[int.wrk.data$internet == 13 ] = 4
int.wrk.data$internet[int.wrk.data$internet == 14 ] = 3
int.wrk.data$internet[int.wrk.data$internet == 15 ] = 2
int.wrk.data$internet[int.wrk.data$internet == 16 ] = 1
int.wrk.data$internet[int.wrk.data$internet == 17 ] = NA

#Reversing the data for tv use where 6 is every day and 0 is no tv access
int.wrk.data$tv[int.wrk.data$tv == 8 ] = NA
int.wrk.data$tv[int.wrk.data$tv == 7 ] = 17
int.wrk.data$tv[int.wrk.data$tv == 6 ] = 16
int.wrk.data$tv[int.wrk.data$tv == 5 ] = 15
int.wrk.data$tv[int.wrk.data$tv == 4 ] = 14
int.wrk.data$tv[int.wrk.data$tv == 3 ] = 13
int.wrk.data$tv[int.wrk.data$tv == 2 ] = 12
int.wrk.data$tv[int.wrk.data$tv == 1 ] = 11

int.wrk.data$tv[int.wrk.data$tv == 11 ] = 6
int.wrk.data$tv[int.wrk.data$tv == 12 ] = 5
int.wrk.data$tv[int.wrk.data$tv == 13 ] = 4
int.wrk.data$tv[int.wrk.data$tv == 14 ] = 3
int.wrk.data$tv[int.wrk.data$tv == 15 ] = 2
int.wrk.data$tv[int.wrk.data$tv == 16 ] = 1
int.wrk.data$tv[int.wrk.data$tv == 17 ] = NA

#Reversing the data for radio use where 6 is every day and 0 is no radio access
int.wrk.data$radio[int.wrk.data$radio == 8 ] = NA
int.wrk.data$radio[int.wrk.data$radio == 7 ] = 17
int.wrk.data$radio[int.wrk.data$radio == 6 ] = 16
int.wrk.data$radio[int.wrk.data$radio == 5 ] = 15
int.wrk.data$radio[int.wrk.data$radio == 4 ] = 14
int.wrk.data$radio[int.wrk.data$radio == 3 ] = 13
int.wrk.data$radio[int.wrk.data$radio == 2 ] = 12
int.wrk.data$radio[int.wrk.data$radio == 1 ] = 11

int.wrk.data$radio[int.wrk.data$radio == 11 ] = 6
int.wrk.data$radio[int.wrk.data$radio == 12 ] = 5
int.wrk.data$radio[int.wrk.data$radio == 13 ] = 4
int.wrk.data$radio[int.wrk.data$radio == 14 ] = 3
int.wrk.data$radio[int.wrk.data$radio == 15 ] = 2
int.wrk.data$radio[int.wrk.data$radio == 16 ] = 1
int.wrk.data$radio[int.wrk.data$radio == 17 ] = NA

#Reversing the data for written.press use where 6 is every day and 0 is no written.press access
int.wrk.data$written.press[int.wrk.data$written.press == 8 ] = NA
int.wrk.data$written.press[int.wrk.data$written.press == 7 ] = 17
int.wrk.data$written.press[int.wrk.data$written.press == 6 ] = 16
int.wrk.data$written.press[int.wrk.data$written.press == 5 ] = 15
int.wrk.data$written.press[int.wrk.data$written.press == 4 ] = 14
int.wrk.data$written.press[int.wrk.data$written.press == 3 ] = 13
int.wrk.data$written.press[int.wrk.data$written.press == 2 ] = 12
int.wrk.data$written.press[int.wrk.data$written.press == 1 ] = 11

int.wrk.data$written.press[int.wrk.data$written.press == 11 ] = 6
int.wrk.data$written.press[int.wrk.data$written.press == 12 ] = 5
int.wrk.data$written.press[int.wrk.data$written.press == 13 ] = 4
int.wrk.data$written.press[int.wrk.data$written.press == 14 ] = 3
int.wrk.data$written.press[int.wrk.data$written.press == 15 ] = 2
int.wrk.data$written.press[int.wrk.data$written.press == 16 ] = 1
int.wrk.data$written.press[int.wrk.data$written.press == 17 ] = NA

#Reversing the data for Social Network use where 6 is every day and 0 is no social network access
int.wrk.data$soc.net[int.wrk.data$soc.net == 8 ] = NA
int.wrk.data$soc.net[int.wrk.data$soc.net == 7 ] = 17
int.wrk.data$soc.net[int.wrk.data$soc.net == 6 ] = 16
int.wrk.data$soc.net[int.wrk.data$soc.net == 5 ] = 15
int.wrk.data$soc.net[int.wrk.data$soc.net == 4 ] = 14
int.wrk.data$soc.net[int.wrk.data$soc.net == 3 ] = 13
int.wrk.data$soc.net[int.wrk.data$soc.net == 2 ] = 12
int.wrk.data$soc.net[int.wrk.data$soc.net == 1 ] = 11

int.wrk.data$soc.net[int.wrk.data$soc.net == 11 ] = 6
int.wrk.data$soc.net[int.wrk.data$soc.net == 12 ] = 5
int.wrk.data$soc.net[int.wrk.data$soc.net == 13 ] = 4
int.wrk.data$soc.net[int.wrk.data$soc.net == 14 ] = 3
int.wrk.data$soc.net[int.wrk.data$soc.net == 15 ] = 2
int.wrk.data$soc.net[int.wrk.data$soc.net == 16 ] = 1
int.wrk.data$soc.net[int.wrk.data$soc.net == 17 ] = NA

#Deleting Years of Education Outliers With < 4 Cases
int.wrk.data$education [int.wrk.data$education > 56 ] = NA

#Removing > 96 y/o due to lack of data for correlation < 4 cases
int.wrk.data$age[int.wrk.data$age > 96 ] = NA

#Setting variables as a factor rather than numeric
int.wrk.data$gender = as.factor(int.wrk.data$gender)
int.wrk.data$occupation = as.factor(int.wrk.data$occupation)

#writing the files for output
write.csv(int.wrk.data, file="int-wrk-data.csv")
write.csv(pers.job.expect.names, file="pers-job-expect-names.csv")
write.csv(economy.expect.names, file="economy-expect-names.csv")
write.csv(media.use.names, file="media-use-names.csv")
write.csv(country.names, file="country.names.csv")

rm(originals)
rm(countries)
save.image()
print("Finished Loading and Cleaning Data")


















