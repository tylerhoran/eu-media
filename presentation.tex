\documentclass{beamer}
\usepackage{amssymb,amsmath,epsfig,epstopdf,tikz,graphicx,amssymb,endnotes,booktabs,caption,fixltx2e,Sweave,apacite,tikz,adjustbox,threeparttable} 

% \usepackage{beamerthemesplit} // Activate for custom appearance

\title{EU Internet Use \& Economic Valuation: Partisanship and Media Influence Across Member States}
\author{Tyler J. Horan \\ Department of Sociology \\ New School for Social Research \\ New York, NY 10003 USA \\ horat351@newschool.edu}
\date{\today}

\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Project Rationale \& Test for Significance}
\begin{itemize}
\item Initial Hypothesis:
\begin{itemize}
\item Internet use affects values of work and Leisure.
\item Effects vary by political and demographic indicators. 
\pause
\item Findings: No real correlation of Internet use with the importance of work ($r_{s}$[38,000] $<$.01) or importance of leisure ($r_{s}$[38,000] $<$.01).
\pause
\end{itemize}
\item Broadening Scope of Hypothesis:
\pause
\begin{itemize}
\item Internet use affects economic evaluations (personal employment and nation's economy). 
\item Effects vary by political and demographic indicators.
\pause
\item Preliminary Findings: Significant correlation between Internet use and evaluation of personal employment ($r_{s}$[26,504] $=$.18) and national economy ($r_{s}$[29,653] $=$.14). 
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Background and Context}
\begin{center}
{\bf Media's Effect on Public Opinion}
\pause
\end{center}
\begin{columns}[l]
\column{2in}
{\it Agenda Setting}
\begin{itemize}
\item Media reflect a reality that's been selectively curated.
\item The frame of content has larger effect than content \cite{cohen1963press}. Effect very high in Television, less so for other media. \cite{iyengar1987news}.
\end{itemize}
\pause
\column{2in}
{\it Cultivation}
\begin{itemize}
\item Media Shape individuals' perception and values through content.
\item High exposure leads to increasingly negative views of the economy and society \cite{pingree1981us,wober1978televised}
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Economic Valuations \& Media Consumption}
\begin{center}
{\bf Measuring Effect Size}
\pause
\end{center}
\begin{columns}[l]
\column{2in} 
{\it Sociotropic Evaluation}
\begin{itemize}
\item Individuals in US make economic evaluations based on national `cues' in media, not personal financial situation \cite{mondak1996persuasion}.
\item Economic content in media is greater predictor of public opinion than actual economic conditions \cite{goidel1995media}.
\end{itemize}
\pause
\column{2in}
{\it Partisan Exposure}
\begin{itemize}
\item Right-leaning exposure has a greater cultivation effect in US - The Fox News Effect \cite{dellavigna2006fox}. 
\item Impact of negative portrayals of economy are greater than positive but amplified for right-leaning individuals due to selective exposure\cite{soroka2006good}.
\end{itemize}
\end{columns}

\end{frame}


\begin{frame}
\frametitle{Lack of Research on Internet Use \& Economic Evaluation}
\begin{itemize}
\item Cultivation effects from Internet consumption are assumed to be the same as Television, Radio and Print News but amplified by increased selective exposure. 
\pause
\item Effects are assumed to be consistent across countries, yet individual values about the state of the economy have not been tested across European countries. 
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Research Questions \& Expected Findings}
\begin{itemize}
\item {\it H1:} Internet use increases the likelihood of an individual positively evaluating their own economic and employment security as well as their country's economic situation. 
\pause
\item {\it H2:} The positive effect is greatest for right-leaning individuals on the political spectrum and for those with less full-time education.
\pause
\item {\it H3:} The positive effect is consistent across member states of the European Union.
\end{itemize}
\end{frame}



\begin{frame}
\vspace{-0.7cm}
\begin{columns}[l]
\column{2.5in}
\begin{tiny}
\frametitle{Summary Statistics for Eurobarometer Variables}
\begin{table}[ht]
\begin{threeparttable}
\caption{Summary Statistics for Eurobarometer Variables}
\begin{tabular}{lrrrrr}
\toprule
 & N & \% & M & SD & Mdn \\
\midrule
Age & 30,778 & & 47.71 & 18.14 & 48.00 \\
Gender & 30,780 & & & \\
\hspace{0.5cm}Male & & 46.8 & & \\
\hspace{0.5cm}Female & & 53.2 & & \\
Education & 27,802 & & & \\
Marital Status & & & & \\
\hspace{0.5cm}Single & & 18.9 & & \\
\hspace{0.5cm}Married & & 65.0 & & \\
\hspace{0.5cm}Divorced & & 7.0 & & \\
\hspace{0.5cm}Widowed & & 9.1 & & \\
Partisanship & 24,797 & & & \\
\hspace{0.5cm}Left & & 30.4 & & \\
\hspace{0.5cm}Center & & 40.9 & & \\
\hspace{0.5cm}Right & & 28.7 & & \\
Job Level & 12,016 & & 2.02 & 1.00 & 3.00 \\
TV Use & 30,743 & & 5.76 & 0.77 & 6.00 \\
Radio Use & 30,686 & & 4.79 & 1.76 & 6.00 \\
Print Use & 30,697 & & 4.41 & 1.76 & 5.00 \\
Internet Use & 30,576 & & 3.72 & 2.44 & 5.00 \\
SM Use$^{\dagger}$ & 30,412 & & 2.45 & 2.18 & 1.00 \\
NE Eval.$^{\dagger\dagger}$ & 29,849 & & -0.34 & 0.53 & -0.33 \\
PE Eval.$^{\dagger\dagger\dagger}$ & 26,643 & & 0.04 & 0.56 & 0.00 \\
\bottomrule
\end{tabular}
\begin{tablenotes}
\item $^{\dagger}$ Online Social Media / Networking Use
\item $^{\dagger\dagger}$ Evaluation of the National Economy
\item $^{\dagger\dagger\dagger}$ Evaluation of Personal Employment Situation
\end{tablenotes}
\end{threeparttable}
\end{table}
\end{tiny}
\pause

\column{2in}
\begin{itemize}
\item Occupation Level on a scale of 1 to 3 (manual labor to manager)
\pause
\item Media use on a scale of 1 to 7 (no access or use to everyday use) 
\pause
\item Economic evaluations on scale of -1 to 1 (very bad to very good)
\end{itemize}



\end{columns}

\end{frame}

\begin{frame}
\frametitle{Assessment of the National Economy as a Function of Internet Use (2010)}
\vspace{-1cm}
\begin{columns}[l]
\column{2.5in}
\begin{small}
\input{./study/nat-econ-politics-pres.tex}
\end{small}

\column{2in}
\vspace{-0.5cm}
\begin{itemize}
\item Internet use increases positive evaluation of an individual's national economy. 
\pause
\item Right-leaning individuals on the political spectrum have a higher economic outlook.
\pause
\item Left-leaning individuals have the greatest change in opinion.
\end{itemize}
\end{columns}
\end{frame}


\begin{frame}
\frametitle{Personal Employment Assessment as a Function of Internet Use (2010)}
\vspace{-1cm}
\begin{columns}[l]
\column{2.5in}
\begin{small}
\input{./study/pers-int-politics-pres.tex}
\end{small}
\column{2in}
\vspace{-0.5cm}
\begin{itemize}
\item Individuals without Internet or who never use the Internet have a slightly negative outlook.
\pause
\item That evaluation becomes positive after Internet use 2-3 time a month.
\pause
\item Does not take into account other variables like age, gender, occupation level or other media use.
\end{itemize}
\end{columns}
\end{frame}




\begin{frame}
\frametitle{Hierarchical Regressions Predicting National Economy Assessment}
\begin{columns}[l]

\column{2.5in}
\begin{tiny}
\begin{table}[ht]
\begin{threeparttable}
\begin{tabular}{llll}
\toprule
 & \multicolumn{3}{l}{\it \hspace{0.6cm}Political Partisanship} \vspace{0.1cm}\\ 

Predictors & Left & Center & Right \\ 
\midrule
{\it Demographics} & & & \\
\hspace{0.5cm} Age & .0027** & .0025 *** & .0022* \\
\hspace{0.5cm} Female & -.0713*** & -.0735*** & -.0850*** \\
\hspace{0.5cm} Education & .0127*** & .0090*** & .0171*** \\
\hspace{0.5cm} Occupation & .0244* & .0171* & .0298** \\
\hspace{0.5cm} Married & -.0360 &-.0282 & -.0132 \\
\hspace{0.5cm} Divorced & -.0433 & -.0382 & -.0101 \\
\hspace{0.5cm} Widowed & .0786 & .0217 & -.0716 \\
\hspace{0.5cm} $R^{2}$ (\%) & 2.6 & 1.5 & 3.8 \\
{\it Traditional Media} & & & \\
\hspace{0.5cm} Television & -.0264* & -.0284* & -.0412** \\
\hspace{0.5cm} Radio & .0191** & .0162** & .0116 \\
\hspace{0.5cm} Print Media & .0486*** & .0370*** & .0391*** \\
\hspace{0.5cm} $\Delta R^{2}$ (\%) & 2.54 & 1.6 & 1.4 \\
{\it New Media} & & & \\
\hspace{0.5cm} Internet & .0281*** & .0171** & .0293*** \\
\hspace{0.5cm} Social Media & -.0039 & -.0069 & -.0078 \\
\hspace{0.5cm} $\Delta R^{2}$ (\%) & 0.83 & 0.24 & 0.65 \\
Total $R^{2}$ (\%) & 6.0 & 3.3 & 5.9 \\
\bottomrule
\end{tabular}
\begin{tablenotes}
      \tiny
      \item *** p $<$ .0001, ** p $<$ .001, * p $<$ .01 
    \end{tablenotes}
\end{threeparttable}

\end{table}
\end{tiny}
\column{2in}
\begin{itemize}
\item Internet use has a positive effect on economic outlook across the political spectrum after controlling for demographics and other media use.
\pause
\item Right-leaning individuals' evaluation of their country's economy is influenced more by demographics than mass media use.
\end{itemize}
\end{columns}
\end{frame}


\begin{frame}
\frametitle{Hierarchical Regressions Predicting National Economy Assessment}
\begin{columns}[l]

\column{2.5in}
\begin{tiny}
\begin{table}[ht]
\begin{threeparttable}
\begin{tabular}{llll}
\toprule
 & \multicolumn{3}{l}{\it \hspace{0.6cm}Political Partisanship} \vspace{0.1cm}\\ 

Predictors & Left & Center & Right \\ 
\midrule
{\it Demographics} & & & \\
\hspace{0.5cm} Age & .0027** & .0025 *** & .0022* \\
\hspace{0.5cm} Female & -.0713*** & -.0735*** & -.0850*** \\
\hspace{0.5cm} Education & .0127*** & .0090*** & .0171*** \\
\hspace{0.5cm} Occupation & .0244* & .0171* & .0298** \\
\hspace{0.5cm} Married & -.0360 &-.0282 & -.0132 \\
\hspace{0.5cm} Divorced & -.0433 & -.0382 & -.0101 \\
\hspace{0.5cm} Widowed & .0786 & .0217 & -.0716 \\
\hspace{0.5cm} $R^{2}$ (\%) & 2.6 & 1.5 & 3.8 \\
{\it Traditional Media} & & & \\
\hspace{0.5cm} Television & -.0264* & -.0284* & -.0412** \\
\hspace{0.5cm} Radio & .0191** & .0162** & .0116 \\
\hspace{0.5cm} Print Media & .0486*** & .0370*** & .0391*** \\
\hspace{0.5cm} $\Delta R^{2}$ (\%) & 2.54 & 1.6 & 1.4 \\
{\it New Media} & & & \\
\hspace{0.5cm} Internet & .0281*** & .0171** & .0293*** \\
\hspace{0.5cm} Social Media & -.0039 & -.0069 & -.0078 \\
\hspace{0.5cm} $\Delta R^{2}$ (\%) & 0.83 & 0.24 & 0.65 \\
Total $R^{2}$ (\%) & 6.0 & 3.3 & 5.9 \\
\bottomrule
\end{tabular}
\begin{tablenotes}
      \tiny
      \item *** p $<$ .0001, ** p $<$ .001, * p $<$ .01 
    \end{tablenotes}
\end{threeparttable}

\end{table}
\end{tiny}
\column{2in}
\begin{itemize}
\item Cultivation effect of traditional media on perception of national economy is greater for left-leaning individuals in EU on the aggregate level, but equal for Internet use.  
\end{itemize}
\end{columns}
\end{frame}




\begin{frame}
\begin{columns}[l]
\column{2.5in}
\frametitle{$R^{2}$ Change With Addition of New Media in Regression of National Economy Assessment}

\begin{tiny}
\begin{table}[ht]

\begin{threeparttable}
\begin{tabular}{lrlr}
  \toprule
Country & $\Delta R^{2}$ (\%) & Country & $\Delta R^{2}$ (\%) \\ 
  \midrule
Belgium & 0.33 & Cyprus & 2.56 \\
Denmark & 0.20 & Czech Rep. & 0.91 \\
Germany West & 1.05 & Estonia & 0.44 \\
Germany East & 1.33 & Hungary & 0.23 \\
Greece & 0.33 & Latvia & -0.40 \\
Spain & 0.18 & Lithuania & 1.20 \\
Finland & 0.20 & Malta & 4.13 \\
France & 0.18 & Poland & 3.41 \\
Ireland & 1.47 & Slovakia & 0.94 \\
Italy & 0.33 & Slovenia & 2.89 \\
Luxembourg & 0.69 & Bulgaria & 0.89 \\
Netherlands & 2.25 & Romania & 1.56 \\
Austria & 0.32 & Turkey & 2.79 \\
Portugal & 0.45 & Croatia & 0.34 \\
Sweden & 0.17 & Macedonia & 0.58 \\
Great Britain & 0.54 & Iceland & 0.63 \\
N. Ireland & 0.22 &  &  \\
  \bottomrule
\end{tabular}
\end{threeparttable}
\end{table}

\end{tiny}

\column{2in}
\begin{itemize}
\item Between-country cultivation effects of new media use are consistently positive, but variable. 
\pause
\item Largest effects are in Malta, Poland, Slovenia and Turkey.
\end{itemize}

\end{columns}

\end{frame}


\begin{frame}
\frametitle{Conclusions}
\begin{itemize}
\item The overall effect of internet use on economic evaluation is positive for both personal and national economic evaluation across the majority of member states within the EU.
\pause
\item While the positive effect of increased Internet use is equal for both left and right-leaning individuals, right leaning individuals have greater optimism about personal and national economic expectations which are more the result of gender, education and occupation level than from media consumption.
\pause
\item The effect of Internet use on national economic evaluation is almost entirely positive but not consistent in magnitude across EU member states.
\end{itemize}
\end{frame}

\begin{frame}
\begin{center}
\LARGE Comments? Questions?

\end{center}
\vspace{0.5in}
\begin{columns}[l]
\hspace{0.5in}
\column{2.5in}
Paper is available at:\\

tylerhoran.com/eurolab.pdf

\column{2.5in}
Source Code is Available at:\\

tylerhoran.com/eurolab.zip

\end{columns}
\end{frame}

\begin{frame}[shrink=30]
\frametitle{References}
\bibliographystyle{apacite}
\small\bibliography{euInternet}
\end{frame}
\end{document}
